import React from 'react';

class PharmaciesGarde extends React.Component{
    constructor(props){
        super(props);
        this.state = {
          pharmaciesArray : [],
        };
    };
    
    componentDidMount(){
        const axios = require('axios');
        axios.defaults.baseURL = process.env.REACT_APP_AXIOS;
        axios.get('/pharma-garde')
            .then(response => {
              this.setState({pharmaciesArray : response.data});
            })
            .catch(function (error) {
              console.log(error);
            });     
    };
   
    render(){
        return (
            <div>
                <h2>Pharmacie(s) de garde aujourd'hui</h2> 
                {this.state.pharmaciesArray.length === 0 && <p>Pas de pharmacie de garde aujourd'hui dans la Région.</p>}
                <ul>{this.state.pharmaciesArray.map(el => (<li key={el.id}><strong>{el.nom}</strong> - {el.quartier} - <em>{el.ville}</em></li>))}</ul>         
            </div>
        )
    }
}

export default PharmaciesGarde;