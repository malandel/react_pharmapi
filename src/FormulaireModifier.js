import React from "react";
import Button from "./Button";
import Pharmacies from "./Pharmacies"

const axios = require('axios');
axios.defaults.baseURL = process.env.REACT_APP_AXIOS;

class FormulaireModifier extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            nom : '',
            quartier:'',
            ville:'',
            garde:'',
            modify : false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    componentDidMount(){
        axios.get('/pharma')
        .then(res => { 
            res.data.map(pharmacie => {
                {pharmacie.id === this.props.id && 
                    this.setState({
                        nom : pharmacie.nom,
                        quartier : pharmacie.quartier,
                        ville : pharmacie.ville,
                        garde : pharmacie.garde
                     })};
            });
            
        })
        .catch(function (error) {
          console.log(error);
        });
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event){
        event.preventDefault();
        axios.put('/pharma/' + this.props.id, { 
            nom : this.state.nom, 
            quartier : this.state.quartier,
            ville : this.state.ville,
            garde : this.state.garde
        })
        .catch(function (error) {
            console.log(error);
          })
        .then(() => {
            alert(`Pharmacie "${this.state.nom}" à ${this.state.ville} bien modifiée.`);
            //On vide le formulaire et le state
            this.setState({
                nom : '',
                quartier : '',
                ville : '',
                garde :'',
                modify : true
            })
        })
    }
    
    render(){
        return(
            <div>
            {this.state.modify === false &&
                <>
                <h2>Modifier pharmacie</h2>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="nom">Nom de la Pharmacie</label>
                    <input type="text" name="nom" value={this.state.nom} onChange={this.handleChange}></input>

                    <label htmlFor="quartier">Quartier</label>
                    <input type="text" name="quartier" value={this.state.quartier} onChange={this.handleChange}></input>

                    <label htmlFor="ville">Ville</label>
                    <input type="text" name="ville" value={this.state.ville} onChange={this.handleChange}></input>

                    <select value={this.state.garde} onChange={this.handleChange} name="garde">
                        <option defaultValue="default">Jour de garde</option>
                        <option value="lundi">Lundi</option>
                        <option value="mardi">Mardi</option>
                        <option value="mercredi">Mercredi</option>
                        <option value="jeudi">Jeudi</option>
                        <option value="vendredi">Vendredi</option>
                        <option value="samedi">Samedi</option>
                        <option value="dimanche">Dimanche</option>
                    </select>
                    <Button title="Modifier" clique={()=>console.log('modifié')}/>
                </form> 
                <Button title="Retour à la liste des pharmacies" clique={()=>this.setState({modify:true})}/>
                </>
            }
            {this.state.modify === true && <Pharmacies/>}
            </div>
        )
    }
}
export default FormulaireModifier;