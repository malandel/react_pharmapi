import React from 'react';
import FormulaireAjout from './FormulaireAjout';

const axios = require('axios');
axios.defaults.baseURL = process.env.REACT_APP_AXIOS;

class AjouterPharmacie extends React.Component{
    
    render(){
        return (
            <div>
                <FormulaireAjout/>
            </div>
        )
    }
}

export default AjouterPharmacie;