import React from 'react';
import './App.css';
import logo from './logo2.svg';
// COMPOSANTS
import Button from './Button';
import Pharmacies from './Pharmacies';
import PharmaciesGarde from './PharmaciesGarde';
import AjouterPharmacie from './AjouterPharmacie';
import Home from './Home';
import Footer from './Footer';



class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      whatButton : 'Home',
    }
  }
    render() {
    return(
      <>
      <header>
        <div id="header_title">
          <div>
            <h1>PHARMAPI</h1>
            <h2>Pour les grands et les petits</h2>
          </div>
          <img src={logo} className="App-logo" alt="logo"/>
        </div>
        <nav>
          <Button title ="Home" clique={()=>this.setState({whatButton : 'Home'})}></Button>
          <Button title="Pharmacies" clique={()=>this.setState({whatButton :'Pharmacies'})}></Button>
          <Button title="Pharmacie(s) de garde" clique={()=>this.setState({whatButton :'Pharmacies de garde'})}></Button>
          <Button title="Ajouter une pharmacie" clique={()=>this.setState({whatButton :'Ajouter une pharmacie'})}></Button>
        </nav>
      </header>
    <main>
    <div className="App">
      {this.state.whatButton === 'Home' && <Home/>}
    </div>
      {this.state.whatButton === 'Pharmacies' && <Pharmacies/>}

      {this.state.whatButton === 'Pharmacies de garde' && <PharmaciesGarde/>}

      {this.state.whatButton === 'Ajouter une pharmacie' && <AjouterPharmacie/>}
    </main>
    <Footer/>
    </>
    );
  }
}

export default App;
