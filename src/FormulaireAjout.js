import React from "react";
import Button from "./Button";
import Pharmacies from "./Pharmacies";

const axios = require('axios');
axios.defaults.baseURL = process.env.REACT_APP_AXIOS;

class FormulaireAjout extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            nom : '',
            quartier:'',
            ville:'',
            garde:'',
            add : false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event){
        event.preventDefault();
        axios.post('/pharma', { 
            nom : this.state.nom, 
            quartier : this.state.quartier,
            ville : this.state.ville,
            garde : this.state.garde
        })
      .then(() => {
        alert(`Pharmacie "${this.state.nom}" à ${this.state.ville} bien enregistrée.`);
        //On vide le formulaire et le state
        this.setState({
            nom : '',
            quartier : '',
            ville : '',
            garde :'',
            add : true
        })
      });
    }
    
    render(){
        return(
            <div>
            {this.state.add === false &&
                <>
                    <h2>Ajouter une pharmacie</h2> 
                    <form onSubmit={this.handleSubmit}>
                        <label htmlFor="nom">Nom de la Pharmacie</label>
                        <input type="text" name="nom" value={this.state.nom} onChange={this.handleChange}></input>

                        <label htmlFor="quartier">Quartier</label>
                        <input type="text" name="quartier" value={this.state.quartier} onChange={this.handleChange}></input>

                        <label htmlFor="ville">Ville</label>
                        <input type="text" name="ville" value={this.state.ville} onChange={this.handleChange}></input>

                        <label htmlFor="garde">Jour de garde</label>
                        <select value={this.state.value} onChange={this.handleChange} name="garde" required>
                            <option defaultValue="default">Jour de garde</option>
                            <option value="lundi">Lundi</option>
                            <option value="mardi">Mardi</option>
                            <option value="mercredi">Mercredi</option>
                            <option value="jeudi">Jeudi</option>
                            <option value="vendredi">Vendredi</option>
                            <option value="samedi">Samedi</option>
                            <option value="dimanche">Dimanche</option>
                        </select>

                        <Button title="Envoyer" clique={()=>console.log('envoyé')}/>
                    </form>
                </>
            }
            {this.state.add === true && <Pharmacies/>} 

            </div>

        )
    }
}
export default FormulaireAjout;