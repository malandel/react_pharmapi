function Home(props) {
    
    return(
        <div id="home">
            <h2>Home</h2>
            <h3>Bienvenue sur le site de Pharmapi</h3>
            <p>Vous trouverez ici la liste des pharmacies de la Région, ainsi que la ou les pharmacies de garde selon le jour.</p>
        </div>
    );
}
export default Home;