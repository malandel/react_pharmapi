import React from 'react';
import Button from './Button';
import FormulaireModifier from './FormulaireModifier';

const axios = require('axios');
axios.defaults.baseURL = process.env.REACT_APP_AXIOS;

class Pharmacies extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      pharmaciesArray: [],
      modify : false,
      idPharma : 0
    };
  };
  
  componentDidMount(){
    this.getPharmaList();
  };

    getPharmaList(){
    axios.get('/pharma')
      .then(response => {
        this.setState({ pharmaciesArray: response.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  suppPharma(id, name){
    axios.delete('/pharma/'+id)
          .then(()=> console.log(`Pharmacie ${id} supprimée`), alert(`${name} supprimée`))
          .catch(function (error) {
            console.log(error);
          });
    // On met à jour le tableau pharmacie dans le state du composant pour éviter d'avoir à faire des requêtes tout le temps 
    this.setState({pharmaciesArray : this.state.pharmaciesArray.filter(pharma => pharma.id !== id)})
  }

  render() {
    return (
      <div>
        {this.state.modify === false &&
          <>
          <h2>Pharmacies</h2>
          {this.state.pharmaciesArray.length === 0 && <p>Chargement de la liste, merci de patienter.</p> }
          <ul>
            {this.state.pharmaciesArray.map(el => (
              <li key={el.id}>
                <strong className="nom_phar">{el.nom}</strong> <br></br>
                <em>Quartier :</em> {el.quartier} <br></br><em>Ville : </em><strong>{el.ville}</strong><br></br>
                <em>Jour de garde :</em> {el.garde} <br></br>
                <Button title="Modifier" clique={() => this.setState({modify : true, idPharma : el.id })}/>
                <Button title="Supprimer" clique={()=>this.suppPharma(el.id, el.nom)} />
              </li>
            ))}
          </ul>
          </>
        }
        {this.state.modify === true && 
        <FormulaireModifier id={this.state.idPharma}/>}
      </div>
    )
  }
}

export default Pharmacies;