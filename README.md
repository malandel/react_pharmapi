## Pharmapi Client avec React

Interface utilisateur de l'API Symfony [Pharmapi](https://pharmapi-api.herokuapp.com/pharma) :

- '/pharma' retourne une liste de pharmacies au format Json
- '/pharma-garde' retourne une ou plusieurs pharmacies si leur jour de garde correspond à la date du jour
- '/pharma/{id}' fonctionne avec la méthodes PUT et DELETE 

Cette application React permet donc d'effectuer le CRUD sur cette API (consulter, modifier, ajouter et supprimer une ou plusieurs pharmacies)

### Adresse en ligne
[Pharmapi](https://react-pharmapi.surge.sh/)

### Maquette

[Figma](https://www.figma.com/file/HrmISRgC56o0lJwdhZjLMB/Pharmapi?node-id=112%3A0)

### Installer le projet en local

- Cloner le projet : ``` git clone https://gitlab.com/malandel/react_pharmapi.git ``` 
- ```cd react_pharmapi && npm run serve``` pour lancer le serveur en mode développement
- PWA : ``` npm run pwa ``` Dans la console du navigateur, aller dans Applications et vérifier que les services workers fonctionnent. Pour héberger la PWA, mettre en ligne à partir du dossier build/ (après avoir fait npm run build)

